//GSAP Animations
jQuery(function($){

    "use strict";

        if ($('section').length > 0) {
            const sectionWidth = document.querySelector("section").offsetWidth;
        }else{
            const sectionWidth = 0;
        }

        function toggleDecadeVisibility() {
          var windowHeight = $(window).height();



          $('.section-overlay').each(function() {

            console.log(window.scrollY, $(this).offset().top)

            var elementTop = $(this).offset().top,
                scrolled = window.scrollY + window.innerHeight / 3 * 2;

            if (elementTop <= scrolled ) {
              // console.log('in viewport');
              //$(this).addClass('element-visible');
              //$(this).width(0)
              $(this).animate({
                width: 0
              }, 1000, "easeInExpo" );
            }
          });


          $('.anim-bot, .anim-bot-big, .anim-right').each(function() {

            // console.log(window.scrollY, $(this).offset().top)

            var elementTop = $(this).offset().top,
                scrolled = window.scrollY + window.innerHeight / 3 * 2;

            if (elementTop <= scrolled ) {
              $(this).addClass('showed');
              /*$(this).animate({
                opacity: 1,
                translateY: 0
              }, 500, "easeInExpo" );*/
            }else{
              // $(this).css('opacity', 0).css('transform', 'translateY(25%)');
            }
          });
        }

        toggleDecadeVisibility();
        $(window).scroll(toggleDecadeVisibility);

        /*gsap.registerPlugin(ScrollTrigger);

        gsap.utils.toArray('.section-overlay').forEach(section => {
            gsap.to(section, {
                scrollTrigger: {
                    trigger: section,
                    start: "top 60%",
                    end: "top 70%",
                },
                width: 0,
                duration: 1,
                ease: "power3.inOut",
            });
        });

        gsap.utils.toArray('.anim-bot').forEach(section => {
            gsap.from(section, {
                scrollTrigger: {
                    trigger: section,
                    start: "top 60%",
                    end: "top 70%",
                },
                y: 25,
                opacity: 0,
                duration: .5,
                ease: "power3.inOut"
            });
        });

        gsap.utils.toArray('.anim-bot-big').forEach(section => {
            gsap.from(section, {
                scrollTrigger: {
                    trigger: section,
                    start: "top 60%",
                    end: "top 70%",
                },
                y: 100,
                opacity: 0,
                duration: 1,
                ease: "power3.inOut"
            });
        });

        gsap.utils.toArray('.anim-right').forEach(section => {
            gsap.from(section, {
                scrollTrigger: {
                    trigger: section,
                    start: "top 60%",
                    end: "top 70%",
                },
                x: 25,
                opacity: 0,
                duration: .5,
                ease: "power3.inOut"
            });
        });

        const heroTL = gsap.timeline();
        heroTL
            .to(".loading-overlay", {
                width: 0,
                opacity: 0,
                duration: 2,
                ease: "power3.inOut"
            })
            .from("header", {
                y: 15,
                opacity: 0,
                duration: .5,
                ease: "power3.inOut"
            })
            .from("#heroSection h6", {
                y: 15,
                opacity: 0,
                duration: .5,
                ease: "power3.inOut"
            })
            .from("#heroSection h1", {
                y: 15,
                opacity: 0,
                duration: .5,
                ease: "power3.inOut"
            })
            .from(".scroll-down", {
                y: 15,
                opacity: 0,
                duration: .5,
                ease: "power3.inOut"
            });*/

        // $('.site-header').css('opacity', 0);
        // $('#heroSection h6').css('opacity', 0);
        // $('#heroSection h1').css('opacity', 0);
        // $('.scroll-down').css('opacity', 0);
        $('.loading-overlay').animate({ width: 0, opacity: 0 }, 1400, "easeInExpo",
          function(){
            // $('.site-header').animate({ opacity: 1 }, 500, "easeInExpo", function(){
            //   $('#heroSection h6').animate({ opacity: 1 }, 500, "easeInExpo", function(){
            //     $('#heroSection h1').animate({ opacity: 1 }, 500, "easeInExpo", function(){
            //       $('.scroll-down').animate({ opacity: 1 }, 500, "easeInExpo")
            //     })
            //   })
            // })

            $('.site-header').addClass('showed');
            setTimeout(function(){
                $('#heroSection h6').addClass('showed');
                setTimeout(function(){
                    $('#heroSection h1').addClass('showed');
                    setTimeout(function(){
                        $('.scroll-down').addClass('showed');
                        $('.scroll-down').addClass('scroll-down');
                        $('.scroll-down').removeClass('scroll-top');
                        //$('.scroll-down .scroll-arrow').css('opacity', 1);
                        //$('.scroll-down h5').css('opacity', 1);
                    }, 500);
                }, 500);
            }, 500);
          }
        );

});

//News Section Animation
jQuery(function($){

    "use strict";

    $('.blog-post').on("mouseenter", function () {
        $(this).addClass('selected').siblings().removeClass('selected');
    });
});





//Scroll Top Button
jQuery(window).scroll(function() {

    "use strict";

    var scroll = jQuery(window).scrollTop();

    if (scroll >= 1) {
        jQuery(".scroll-down").addClass("scroll-top");
        jQuery(".scroll-down a").attr("href", "#heroSection");
    } else {
        jQuery(".scroll-down").removeClass("scroll-top");
        jQuery(".scroll-down a").removeAttr("href");
    }

    if(scroll >= 1000) {
        jQuery(".scroll-top").addClass("scroll-top-active");
    } else {
        jQuery(".scroll-top").removeClass("scroll-top-active");
    }
});



//Portfolio Filters
jQuery(function($){

    "use strict";

  $('.nav-item').on("click", function(){
    $('.nav-item').removeClass("active");
    $(this).addClass("active");
    return false;
  });
  
  
  $(function() {
    var selectedClass = "";
    $(".nav-item").on("click", function(){
      selectedClass = $(this).attr("data-rel");
            $(".portfolio-row .portfolio-box").addClass('portfolio-hidden');
            $(".portfolio-row .portfolio-box." + selectedClass).removeClass('portfolio-hidden');
    });
  });
  
});