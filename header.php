<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 * @author Artem Horbenko <artemhorbenko@gmail.com>
 * @package Strada
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class( 'elementorLayoutStrada' ); ?>>
<?php wp_body_open(); ?>
<!-- <div id="page" class="site"> -->
	<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'strada' ); ?></a>

	<!---------- Loading Overlay ---------->
	<div class="loading-overlay"></div>

	<!---------- Scroll Down ---------->
	<div class="scroll-down">
		<h5>Scroll Down</h5>
		<div class="scroll-arrow">
			<a>
				<span class="line line-h"></span>
				<span class="line line-1"></span>
				<span class="line line-2"></span>
			</a>
		</div>
	</div>

	<!---------- Header Start ---------->
	<header id="masthead" class="site-header">

		<div class="container-fluid">

			<div class="row">

				<div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 header-left">

					<?php
					if ( has_custom_logo() ) {
						the_custom_logo();
						?>
						<!-- TODO -->
						<!-- <div class="logo">
							<a href="index.html"><span>STRÅDA</span></a>
						</div> -->
						<?php
					} else {
						?>
						<div class="logo">
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><span><?php bloginfo( 'name' ); ?></span></a>
						</div>
						<?php
					}
					?>

				</div>

				<div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8 header-right">
					<?php
					wp_nav_menu(
						array(
							'theme_location' => 'primary-menu',
							'menu_id'        => 'primary-menu',
							'menu_class'     => 'main-menu',
						)
					);
					?>

				</div>

			</div>

		</div>

	</header>
	<!---------- Header End ---------->
