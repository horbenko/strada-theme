<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 * @author Artem Horbenko <artemhorbenko@gmail.com>
 * @package Strada
 */

?>

	<!---------- Footer Section ---------->
	<footer id="footerSection">

		<div class="section-overlay"></div>

		<div class="container-fluid-small">

			<div class="row">

				<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-9 footer-title anim-bot">
					<?php
					$strada_footer_cta_copy = get_theme_mod( 'footer_cta_copy' );
					if ( $strada_footer_cta_copy ) {
						echo '<h2 class="big-title">' . esc_html( $strada_footer_cta_copy ) . '</h2>';
					}
					?>
				</div>

				<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-3 footer-project anim-bot">
					<?php
					$strada_footer_cta_link_title = get_theme_mod( 'footer_cta_link_title' );
					$strada_footer_cta_link_url   = get_theme_mod( 'footer_cta_link_url' );
					if ( $strada_footer_cta_link_title && $strada_footer_cta_link_url ) {
						echo '<a href="' . esc_url( $strada_footer_cta_link_url ) . '">' . esc_html( $strada_footer_cta_link_title ) . '</a>';
					}
					?>
				</div>

			</div>

			<div class="row">

				<div class="col-12 col-sm-12 col-md-12 col-lg-11 col-xl-4 copyright">
					<?php
					$strada_footer_copyright = get_theme_mod( 'footer_copyright_copy' ) ? get_theme_mod( 'footer_copyright_copy' ) : 'Copyright © 2021 Strada Creative.<br>Built with love by raduCio & AH.';
					echo '<p>' . esc_html( $strada_footer_copyright ) . '</p>';
					?>
					<div class="footer-menu">
						<?php
						wp_nav_menu(
							array(
								'theme_location' => 'socials-menu',
								'menu_id'        => 'socials-menu',
							)
						);
						?>
					</div>
				</div>

				<div class="col-1 footer-offset"></div>

				<?php dynamic_sidebar( 'footer-1' ); ?>

			</div>

		</div>

	</footer>
	<!---------- Footer End ---------->

	<!-- TODO -->
	<!-- <footer id="colophon" class="site-footer">
		<div class="site-info">
			<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'strada' ) ); ?>">
				<?php
				/* translators: %s: CMS name, i.e. WordPress. */
				printf( esc_html__( 'Proudly powered by %s', 'strada' ), 'WordPress' );
				?>
			</a>
			<span class="sep"> | </span>
				<?php
				/* translators: 1: Theme name, 2: Theme author. */
				printf( esc_html__( 'Theme: %1$s by %2$s.', 'strada' ), 'strada', '<a href="https://horbenko.com">Artem Horbenko</a>' );
				?>
		</div>
	</footer> -->
<!-- </div> --><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
