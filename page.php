<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 * @author Artem Horbenko <artemhorbenko@gmail.com>
 * @package Strada
 */

get_header();
?>

	<!---------- Hero Start ---------->
	<section id="heroSection" class="section parallax-top-section">

		<div class="container-fluid-small">

			<div class="row">

				<div class="col-12">
					<?php the_title( '<h1 class="entry-title big-title">', '</h1>' ); ?>
				</div>

			</div>

		</div>

	</section>
	<!---------- Hero End ---------->

	<main id="primary" class="site-main">
		<section class="section">
			<div class="container-fluid-small">
				<?php
				while ( have_posts() ) :
					the_post();

					get_template_part( 'template-parts/content', 'page' );

				endwhile; // End of the loop.
				?>
			</div>
		</section>

		<?php
		// If comments are open or we have at least one comment, load up the comment template.
		if ( comments_open() || get_comments_number() ) :
			comments_template();
		endif;
		?>

	</main><!-- #main -->

<?php
get_footer();
