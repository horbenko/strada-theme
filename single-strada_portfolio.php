<?php
/**
 * The template for displaying all single custom post types elysio_portfolio.
 *
 * @author Artem Horbenko <artemhorbenko@gmail.com>
 * @package elysio-architect
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();

?>

	<!---------- Hero Start ---------->
	<section id="heroSection" class="single-project-hero section" style="background-image: url(<?php echo esc_url( get_the_post_thumbnail_url( $post->ID, 'full' ) ); ?>) !important;">

		<div class="dark-bg"></div>

		<div class="container-fluid-small">

			<div class="row">

				<div class="col-12 single-hero-text">
					<h6 class="sub-title"><?php echo esc_html( custom_taxonomies_terms_links() ); ?></h6>
					<?php the_title( '<h1 class="big-title entry-title">', '</h1>' ); ?>
				</div>

			</div>

		</div>

	</section>
	<!---------- Hero End ---------->

	<?php
	while ( have_posts() ) :
		the_post();
		the_content();
	endwhile; // End of the loop.
	?>

	<!---------- Project Next Start ---------->
	<section id="projectNext" class="section">

		<div class="container-fluid-small">

			<div class="row">

				<div class="col-12 project-nav">
					<h1 class="big-title"><?php previous_post_link( '%link', 'Next project' ); ?></h1>
					<?php if ( strlen( get_previous_post()->post_title ) === 0 ) { ?>
						<h1 class="big-title next-grey">Last project</h1>
					<?php } ?>
				</div>

			</div>

		</div>

	</section>
	<!---------- Project Next End ---------->


<?php get_footer( 'blank' ); ?>
