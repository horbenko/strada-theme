<?php
/**
 * Strada functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 * @author Artem Horbenko <artemhorbenko@gmail.com>
 * @package Strada
 */

if ( ! defined( 'STRADA_VERSION' ) ) {
	define( 'STRADA_VERSION', '1.0.0' );
}

if ( ! function_exists( 'strada_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function strada_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Strada, use a find and replace
		 * to change 'strada' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'strada', get_template_directory() . '/languages' );

		/* Add default posts and comments RSS feed links to head. */
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		add_image_size( 'blog-thumb', 290, 194, array( 'center', 'center' ) );

		/* This theme uses wp_nav_menu() in one location. */
		register_nav_menus(
			array(
				'primary-menu' => esc_html__( 'Primary', 'strada' ),
				'socials-menu' => esc_html__( 'Socials', 'strada' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		/* Set up the WordPress core custom background feature. */
		add_theme_support(
			'custom-background',
			apply_filters(
				'strada_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		/* Add theme support for selective refresh for widgets. */
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'strada_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function strada_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'strada_content_width', 640 );
}
add_action( 'after_setup_theme', 'strada_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function strada_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'strada' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'strada' ),
			'before_widget' => '<section id="%1$s" class="widget anim-bot-big %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h5 class="widget-title sub-title">',
			'after_title'   => '</h5>',
		)
	);
	register_sidebar(
		array(
			'name'          => esc_html__( 'Footer', 'strada' ),
			'id'            => 'footer-1',
			'description'   => esc_html__( 'Add widgets here.', 'strada' ),
			'before_widget' => '<section id="%1$s" class="widget col-12 col-sm-12 col-md-6 col-lg-6 col-xl-3 %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h5 class="widget-title sub-title">',
			'after_title'   => '</h5>',
		)
	);
}
add_action( 'widgets_init', 'strada_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function strada_scripts() {
	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/assets/styles/bootstrap.css', array(), STRADA_VERSION );
	wp_enqueue_style( 'strada', get_template_directory_uri() . '/assets/css/style.css', array( 'bootstrap' ), STRADA_VERSION );
	wp_enqueue_style( 'strada-icofont', get_template_directory_uri() . '/assets/icons/icofont.min.css', array(), STRADA_VERSION );
	wp_enqueue_style( 'style', get_template_directory_uri() . '/style.css', array( 'bootstrap', 'strada' ), STRADA_VERSION );

	wp_enqueue_script( 'strada-navigation', get_template_directory_uri() . '/js/navigation.js', array(), STRADA_VERSION, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		/* Overwrite  'comment-reply' by custom js */
		wp_enqueue_script( 'strada-comment-reply', get_template_directory_uri() . '/js/comment-reply.js', array(), STRADA_VERSION, true );

	}

	wp_enqueue_script( 'strada-bootstrap', get_template_directory_uri() . '/assets/scripts/bootstrap.min.js', array( 'jquery' ), STRADA_VERSION, true );
	wp_enqueue_script( 'jquery-easing', get_template_directory_uri() . '/assets/scripts/jquery.easing.min.js', array( 'jquery' ), STRADA_VERSION, true );
	wp_enqueue_script( 'strada-ScrollTrigger', get_template_directory_uri() . '/assets/scripts/ScrollTrigger.min.js', array( 'jquery' ), STRADA_VERSION, true );
	wp_enqueue_script( 'strada-scripts', get_template_directory_uri() . '/assets/scripts/scripts.js', array( 'jquery', 'jquery-easing' ), STRADA_VERSION, true );

}
add_action( 'wp_enqueue_scripts', 'strada_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Load TGMPA compatibility files.
 */
require_once get_parent_theme_file_path( '/inc/tgmpa-register.php' );

/**
 * Load MerlinWP compatibility files.
 */
require_once get_parent_theme_file_path( '/inc/merlin/vendor/autoload.php' );
require_once get_parent_theme_file_path( '/inc/merlin/class-merlin.php' );
require_once get_parent_theme_file_path( '/inc/merlin-config.php' );

/**
 * Declare demo data files 
 */
function strada_merlin_local_import_files() {
	return array(
		array(
			'import_file_name'             => 'Demo Import',
			'local_import_file'            => get_parent_theme_file_path( '/inc/demo/content.xml' ),
			'local_import_widget_file'     => get_parent_theme_file_path( '/inc/demo/widgets.wie' ),
			'local_import_customizer_file' => get_parent_theme_file_path( '/inc/demo/customizer.dat' ),
			'import_preview_image_url'     => 'http://www.your_domain.com/merlin/preview_import_image1.jpg',
			'import_notice'                => __( 'A special note for this import.', 'strada' ),
			'preview_url'                  => 'https://strada.horbenko.com',
		),
	);
}
add_filter( 'merlin_import_files', 'strada_merlin_local_import_files' );

/**
 * Helper function: disable elementor redirect to welcome page after first active. Because it broke MerliWP.
 */
function strada_remove_elementor_splash() { 
	delete_transient( 'elementor_activation_redirect' );
}
add_action( 'init', 'strada_remove_elementor_splash' );
