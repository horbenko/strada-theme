<?php
/**
 * The template for displaying comments
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 * @author Artem Horbenko <artemhorbenko@gmail.com>
 * @package Strada
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}


function mytheme_comment( $comment, $args, $depth ) {
	if ( 'div' === $args['style'] ) {
		$tag       = 'div';
		$add_below = 'comment';
	} else {
		$tag       = 'li';
		$add_below = 'div-comment';
	}

	$classes = ' ' . comment_class( empty( $args['has_children'] ) ? '' : 'parent', null, null, false );
	?>

	<<?php echo $tag, $classes; ?> id="comment-<?php comment_ID() ?>">
	<?php if ( 'div' != $args['style'] ) { ?>
		<div id="div-comment-<?php comment_ID() ?>" class="comment-body"><?php
	} ?>

	<div class="comment-author vcard">
		<?php
		if ( $args['avatar_size'] != 0 ) {
			echo get_avatar( $comment, $args['avatar_size'] );
		}
		printf(
			__( '<cite class="fn">%s</cite> <span class="says">says:</span>' ),
			get_comment_author_link()
		);
		?>
	</div>

	<?php if ( $comment->comment_approved == '0' ) { ?>
		<em class="comment-awaiting-moderation">
			<?php _e( 'Your comment is awaiting moderation.' ); ?>
		</em><br/>
	<?php } ?>

	<div class="comment-meta commentmetadata">
		<a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ); ?>">
			<?php
			printf(
				__( '%1$s at %2$s' ),
				get_comment_date(),
				get_comment_time()
			); ?>
		</a>

		<?php edit_comment_link( __( '(Edit)' ), '  ', '' ); ?>
	</div>

	<?php comment_text(); ?>

	<div class="reply">
		<?php
		comment_reply_link(
			array_merge(
				$args,
				array(
					'add_below' => $add_below,
					'depth'     => $depth,
					'max_depth' => $args['max_depth']
				)
			)
		); ?>
	</div>

	<?php if ( 'div' != $args['style'] ) { ?>
		</div>
	<?php }
}

?>

<section id="comments" class="comments-area">

	<div class="section-overlay"></div>

	<div class="container-fluid-small">

		<div class="row">

			<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-6 comments-form anim-right">
				<?php

				$commenter = wp_get_current_commenter();
				$req = true;
				$aria_req = ( $req ? " aria-required='true'" : '' );

				$comment_form_args = [
					'fields'               => [
						'author' => '<p class="comment-form-author">
							<input placeholder="' . __( 'Name' ) . ( $req ? '*' : '' ) . ' :" id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . $html_req . ' />
						</p>',
						'email'  => '<p class="comment-form-email">
							<input placeholder="' . __( 'Email' ) . ( $req ? '*' : '' ) . ' :" id="email" name="email" ' . 'type="email"' . ' value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30" aria-describedby="email-notes"' . $aria_req . $html_req  . ' />
						</p>',
						'cookies' => '<p class="comment-form-cookies-consent">'.
							 sprintf( '<input id="wp-comment-cookies-consent" name="wp-comment-cookies-consent" type="checkbox" value="yes"%s />', $consent ) .'
							 <label for="wp-comment-cookies-consent">'. __( 'Save my name, email, and website in this browser for the next time I comment.' ) .'</label>
						</p>',        
					],
					'comment_field'        => '<p class="comment-form-comment">
						<textarea placeholder="' . _x( 'Comment', 'noun' ) . '* :" id="comment" name="comment" rows="4"  aria-required="true" required="required"></textarea>
					</p>',
					'must_log_in'          => '<p class="must-log-in">' . 
						 sprintf( __( 'You must be <a href="%s">logged in</a> to post a comment.' ), wp_login_url( apply_filters( 'the_permalink', get_permalink( $post_id ) ) ) ) . '
					 </p>',
					'logged_in_as'         => '<p class="logged-in-as">' . 
						 sprintf( __( '<a href="%1$s" aria-label="Logged in as %2$s. Edit your profile.">Logged in as %2$s</a>. <a href="%3$s">Log out?</a>' ), get_edit_user_link(), $user_identity, wp_logout_url( apply_filters( 'the_permalink', get_permalink( $post_id ) ) ) ) . '
					 </p>',
					'comment_notes_before' => '<p class="comment-notes">
						<span id="email-notes">' . __( 'Your email address will not be published. Required fields are marked *' ) . '</span>'. 
						( $req ? $required_text : '' ) . '
					</p>',
					'comment_notes_after'  => '',
					'id_form'              => 'commentform',
					'id_submit'            => 'submit',
					'class_form'           => 'comment-form',
					'class_submit'         => 'submit',
					'name_submit'          => 'submit',
					'title_reply'          => __( 'Leave a comment' ),
					'title_reply_to'       => __( 'Leave a reply to %s' ),
					'title_reply_before'   => '<h2 id="reply-title" class="comment-reply-title big-title">',
					'title_reply_after'    => '</h2>',
					'cancel_reply_before'  => ' <small>',
					'cancel_reply_after'   => '</small>',
					'cancel_reply_link'    => __( 'Cancel reply' ),
					'label_submit'         => __( 'Post Comment' ),
					'submit_button'        => '<input name="%1$s" type="submit" id="%2$s" class="%3$s" value="%4$s" />',
					'submit_field'         => '<p class="form-submit">%1$s %2$s</p>',
					'format'               => 'xhtml',
				];

				add_filter('comment_form_fields', 'kama_reorder_comment_fields' );
				function kama_reorder_comment_fields( $fields ){
					// die(print_r( $fields )); // посмотрим какие поля есть

					$new_fields = array(); // сюда соберем поля в новом порядке

					$myorder = array('author','email','comment'); // нужный порядок

					foreach( $myorder as $key ){
						$new_fields[ $key ] = $fields[ $key ];
						unset( $fields[ $key ] );
					}

					// если остались еще какие-то поля добавим их в конец
					if( $fields )
						foreach( $fields as $key => $val )
							$new_fields[ $key ] = $val;

					return $new_fields;
				}

				comment_form( $comment_form_args );

				//comment_form();
				?>
			</div>

			<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-6 comments-list anim-right">

				<?php
				// You can start editing here -- including this comment!
				if ( have_comments() ) :
					?>
					<h2 class="comments-title big-title">
						<?php
						$strada_comment_count = get_comments_number();
						if ( '1' === $strada_comment_count ) {
							printf(
								/* translators: 1: title. */
								esc_html__( 'One thought on &ldquo;%1$s&rdquo;', 'strada' ),
								'<span>' . wp_kses_post( get_the_title() ) . '</span>'
							);
						} else {
							printf( 
								/* translators: 1: comment count number, 2: title. */
								esc_html( _nx( '%1$s thought on &ldquo;%2$s&rdquo;', '%1$s thoughts on &ldquo;%2$s&rdquo;', $strada_comment_count, 'comments title', 'strada' ) ),
								number_format_i18n( $strada_comment_count ), // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
								'<span>' . wp_kses_post( get_the_title() ) . '</span>'
							);
						}
						?>
					</h2>
					<!-- .comments-title -->

					<?php the_comments_navigation(); ?>

					<ol class="comment-list">
						<?php
						wp_list_comments(
							array(
								'style'      => 'ol',
								'short_ping' => true,
							)
						);
						?>
					</ol><!-- .comment-list -->



					<!-- <ul class="commentlist"> -->
						  <?php //wp_list_comments('type=comment&callback=mytheme_comment'); ?>
					<!-- </ul> -->


					<?php
					the_comments_navigation();

					// If comments are closed and there are comments, let's leave a little note, shall we?
					if ( ! comments_open() ) :
						?>
						<p class="no-comments"><?php esc_html_e( 'Comments are closed.', 'strada' ); ?></p>
						<?php
					endif;
				else:
					?>

					<br>
					<p><?php esc_html_e( 'No comments yet. Be the first to share what you think.', 'strada' ); ?></p>

					<?php
				endif; // Check for have_comments().
				?>
			</div>
		</div>
	</div>

</section><!-- #comments -->
