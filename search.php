<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 * @author Artem Horbenko <artemhorbenko@gmail.com>
 * @package Strada
 */

get_header();
?>

	<!---------- Hero Start ---------->
	<section id="heroSection" class="simple-hero section">

		<div class="container-fluid-small">

			<div class="row">

				<div class="col-12">
					<h6 class="sub-title">Search Results For <?php echo get_search_query(); ?></h6>
					<h1>
						<!-- <input type="search" placeholder="Type and hit enter..."> -->
						<?php get_search_form(); ?>
					</h1>
				</div>

			</div>

		</div>

	</section>
	<!---------- Hero End ---------->

	<!---------- News Start ---------->
	<section id="newsSection" class="section">

		<div class="container-fluid-small">

			<div class="row">

				<?php if ( have_posts() ) : ?>
					<?php
					/* Start the Loop */
					while ( have_posts() ) :
						the_post();

						/*
						 * Include the Post-Type-specific template for the content.
						 * If you want to override this in a child theme, then include a file
						 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
						 */
						get_template_part( 'template-parts/content-news', get_post_type() );

					endwhile;

					strada_pagintion();

				else :

					get_template_part( 'template-parts/content', 'none' );

				endif;
				?>

			</div>

		</div>

	</section>
	<!---------- News End ---------->

<?php
get_footer();
