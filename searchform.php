<?php
/**
 * Theme search form. 
 * 
 * @link https://developer.wordpress.org/reference/functions/get_search_form/
 * @author Artem Horbenko <artemhorbenko@gmail.com>
 * @package Strada
 */

?>

<form id="searchForm" role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<label>
		<span class="screen-reader-text"><?php echo esc_html_x( 'Search for:', 'label', 'strada' ); ?></span>
		<input type="search" class="search-field" placeholder="<?php echo esc_attr_x( 'Type and hit enter...', 'placeholder', 'strada' ); ?>" value="<?php echo get_search_query(); ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label', 'strada' ); ?>" />
	</label>
	<!-- <input type="submit" class="search-submit" value="<?php echo esc_attr_x( 'Search', 'submit button', 'strada' ); ?>" /> -->
</form>
