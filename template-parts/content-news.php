<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 * @author Artem Horbenko <artemhorbenko@gmail.com>
 * @package Strada
 */

$strada_post_classes = 'col-12 blog-post anim-bot-big';
if ( is_sticky() ) {
	$strada_post_classes .= ' sticky';
}

$strada_title_classes = 'big-title entry-title';

if ( strlen( get_the_title() ) > 60 ) {
	$strada_title_classes .= ' break-word';
}
?>

<article id="post-<?php the_ID(); ?>" <?php post_class( $strada_post_classes ); ?>>

	<a href="<?php the_permalink(); ?>">
		<div class="row">

			<div class="col-12 col-sm-12 col-md-12 col-lg-2 col-xl-2 blog-date">
				<h5 class="sub-title"><?php strada_posted_data(); ?></h5>
				<?php
				if ( is_sticky() ) :
					?>
					<h5 class="sub-title featured">Featured</h5>
					<?php
				endif;
				?>
			</div>

			<div class="col-12 col-sm-12 col-md-12 col-lg-7 col-xl-7 blog-title">
				<?php
				the_title( '<h2 class="' . $strada_title_classes . '">', '</h2>' );
				?>
			</div>

			<div class="col-12 col-sm-12 col-md-12 col-lg-3 col-xl-3 blog-image">
				<?php
				if ( has_post_thumbnail() ) {
					the_post_thumbnail( 'blog-thumb' );
				} else {
					?>
					<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/blog-transparent.png" tabindex="-1">
					<?php
				}
				?>
			</div>
		</div>
	</a>
</article>
