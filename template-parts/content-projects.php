<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 * @author Artem Horbenko <artemhorbenko@gmail.com>
 * @package Strada
 */

?>

<div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 portfolio-box anim-bot-big">
	<div class="cancel-anchor"></div>
	<a href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark">
		<div class="portfolio-image">
			<div class="dark-bg"></div>
			<img src="<?php echo esc_url( get_the_post_thumbnail_url( $post->ID, 'full' ) ); ?>">
			<div class="portfolio-text">
				<h3><?php the_title(); ?></h3>
				<h6><?php echo esc_html( custom_taxonomies_terms_links() ); ?></h6>
			</div>
		</div>
	</a>
</div>
