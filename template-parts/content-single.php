<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 * @author Artem Horbenko <artemhorbenko@gmail.com>
 * @package Strada
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="anim-bot-big">
		<?php strada_post_thumbnail(); ?>
	</div>

	<div class="entry-content">
		<?php
		the_content(
			sprintf(
				wp_kses(
					/* translators: %s: Name of current post. Only visible to screen readers */
					__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'strada' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				wp_kses_post( get_the_title() )
			)
		);
		?>
		<div class="clearfix"></div>
		<?php
		wp_link_pages(
			array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'strada' ),
				'after'  => '</div>',
			)
		);
		?>
	</div><!-- .entry-content -->

	<div class="anim-right">
		<p class="tags"><?php strada_entry_tags(); ?></p>
	</div>

</article><!-- #post-<?php the_ID(); ?> -->
