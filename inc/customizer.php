<?php
/**
 * Strada Theme Customizer
 *
 * @author Artem Horbenko <artemhorbenko@gmail.com>
 * @package Strada
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function strada_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport        = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport = 'postMessage';
	// $wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
	$wp_customize->remove_section( 'colors' );

	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial(
			'blogname',
			array(
				'selector'        => '.site-title a',
				'render_callback' => 'strada_customize_partial_blogname',
			)
		);
		$wp_customize->selective_refresh->add_partial(
			'blogdescription',
			array(
				'selector'        => '.site-description',
				'render_callback' => 'strada_customize_partial_blogdescription',
			)
		);
	}






	/**
	 * Select sanitization function
	 *
	 * @param string               $input   Slug to sanitize.
	 * @param WP_Customize_Setting $setting Setting instance.
	 * @return string Sanitized slug if it is a valid choice; otherwise, the setting default.
	 */
	function strada_theme_slug_sanitize_select( $input, $setting ) {

		// Ensure input is a slug (lowercase alphanumeric characters, dashes and underscores are allowed only).
		$input = sanitize_key( $input );

		// Get the list of possible select options.
		$choices = $setting->manager->get_control( $setting->id )->choices;

		// If the input is a valid key, return it; otherwise, return the default.
		return ( array_key_exists( $input, $choices ) ? $input : $setting->default );

	}



	// Theme style settings.
	$wp_customize->add_section(
		'strada_theme_options',
		array(
			'title'       => __( 'Theme Options', 'strada' ),
			'capability'  => 'edit_theme_options',
			'priority'    => apply_filters( 'strada_theme_options_priority', 160 ),
		)
	);


	$section = 'strada_theme_options';






	$setting = 'primary_color';
	$wp_customize->add_setting(
		$setting, 
		array(
			'default'     => '#007bff',
			'transport'   => $transport,
		)
	);
	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			$setting,
			array(
				'label'       => __( 'Primary Color', 'strada' ),
				'description' => __( 'For links, buttons and primary accent.', 'strada' ),
				'section'     => $section,
				'settings'    => $setting,
				'priority'    => 1,
			)
		)
	);



	/* Font Family */

	$setting = 'strada_theme_font';
	$wp_customize->add_setting( $setting, array(
	  'capability' => 'edit_theme_options',
	  'default' => 'Inter',
	) );
	$wp_customize->add_control( $setting, array(
	  'type' => 'select',
	  'section' => $section, // Add a default or your own section
	  'label' => __( 'Font Family' ),
	  // 'description' => __( 'This is a custom select option.' ),
	  'choices' => array(
	    'Inter' => __( 'Inter' ),
	    'Roboto' => __( 'Roboto' ),
	    'Roboto+Condensed' => __( 'Roboto Condensed' ),
	    'Source+Sans+Pro' => __( 'Source Sans Pro' ),
	    'Open+Sans' => __( 'Open Sans' ),
	    'Oswald' => __( 'Oswald' ),
	    'Dela+Gothic+One' => __( 'Dela Gothic One' ),
	    'Noto+Sans' => __( 'Noto Sans' ),
	    'Noto+Sans+JP' => __( 'Noto Sans JP' ),
	    'Lato' => __( 'Lato' ),
	    'Montserrat' => __( 'Montserrat' ),
	    'Poppins' => __( 'Poppins' ),
	    'Lexend' => __( 'Lexend' ),
	    'Raleway' => __( 'Raleway' ),
	    'Roboto+Mono' => __( 'Roboto Mono' ),
	    'PT+Sans' => __( 'PT Sans' ),
	    'Merriweather' => __( 'Merriweather' ),
	    'Roboto+Slab' => __( 'Roboto Slab' ),
	    'Ubuntu' => __( 'Ubuntu' ),
	    'Playfair+Display' => __( 'Playfair Display' ),
	    'Nunito' => __( 'Nunito' ),
	    'Custom' => __( 'Custom Google Font' ),
	  ),
	) );

	$setting = 'strada_theme_font_link';
	$wp_customize->add_setting( $setting, [
		'default'            => '',
		// 'placeholder'	=> 'https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600;700;800;900&display=swap',
		'sanitize_callback'    => 'sanitize_text_field',
		'transport'          => $transport
	] );
	$wp_customize->add_control( $setting, [
		'section'  => $section,
		'label'    => 'Custom Google Font',
		'type'     => 'text',
		'description'	=> 'Check <a href="https://fonts.google.com/" target="_blnk">fonts.google.com</a> for more fonts.',
	] );

	// $setting = 'strada_custom_font';
	// $wp_customize->add_setting( $setting, [
	// 	'default'            => '',
	// 	//'sanitize_callback'  => 'sanitize_text_field',
	// 	// 'transport'          => $transport
	// ] );
	// $wp_customize->add_control( $setting, [
	// 	'section'  => $section,
	// 	'label'    => __('Custom Font Family', 'strada' ),
	// 	'type'     => 'text'
	// ] );







	$setting = 'footer_copyright_copy';
	$wp_customize->add_setting( $setting, [
		'default'            => 'Copyright © 2020 Strada Creative.<br>Built with love by raduCio.',
		//'sanitize_callback'  => 'sanitize_text_field',
		// 'transport'          => $transport
	] );
	$wp_customize->add_control( $setting, [
		'section'  => $section,
		'label'    => __('Footer Copyright Text', 'strada' ),
		'type'     => 'text'
	] );


	$setting = 'footer_cta_copy';
	$wp_customize->add_setting( $setting, [
		'default'            => 'Let\'s work together.',
		//'sanitize_callback'  => 'sanitize_text_field',
		// 'transport'          => $transport
	] );
	$wp_customize->add_control( $setting, [
		'section'  => $section,
		'label'    => __('Footer CTA Text', 'strada' ),
		'type'     => 'text'
	] );

	$setting = 'footer_cta_link_title';
	$wp_customize->add_setting( $setting, [
		'default'            => 'Start a project',
		//'sanitize_callback'  => 'sanitize_text_field',
		// 'transport'          => $transport
	] );
	$wp_customize->add_control( $setting, [
		'section'  => $section,
		'label'    => __('Footer CTA Link Title', 'strada' ),
		'type'     => 'text'
	] );

	$setting = 'footer_cta_link_url';
	$wp_customize->add_setting( $setting, [
		'default'            => '#',
		//'sanitize_callback'  => 'sanitize_text_field',
		// 'transport'          => $transport
	] );
	$wp_customize->add_control( $setting, [
		'section'  => $section,
		'label'    => __('Footer CTA Link URL', 'strada' ),
		'type'     => 'text'
	] );

}
add_action( 'customize_register', 'strada_customize_register' );

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function strada_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function strada_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function strada_customize_preview_js() {
	wp_enqueue_script( 'strada-customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), STRADA_VERSION, true );
}
add_action( 'customize_preview_init', 'strada_customize_preview_js' );









function customizer_style_tag(){

// 	/* Fonts */

// 	if( $strada_theme_font_link = get_theme_mod( 'strada_theme_font_link' ) ){
// 		wp_enqueue_style( 'strada-gfonts', $strada_theme_font_link, false, false );
// 	}else{
// 		wp_enqueue_style( 'strada-gfonts', 'https://fonts.googleapis.com/css2?family=' . get_theme_mod( 'strada_theme_font' ) . ':wght@100;200;300;400;500;600;700;800;900&display=swap', false, false );
// 	}

	$style = [];

// 	$body_styles = [];

// 	if( get_theme_mod( 'strada_theme_font' ) ){
// 		$body_styles[] = 'font-family: ' . get_theme_mod( 'strada_theme_font' );
// 	}else{
// 		$body_styles[] = 'font-family: "Inter"';
// 	}

	// $style[] = 'body { '. implode( ' ', $body_styles ) .' }';



	// Links
	$style[] = 'a { color: ' . get_theme_mod( 'primary_color' ) . '; }';
	$style[] = 'a:after { background: ' . get_theme_mod( 'primary_color' ) . '; }';

	// Buttons
	$style[] = '#comments .comments-form input[type=submit], #commentsSection .comments-form input[type=submit] { border-color: ' . get_theme_mod( 'primary_color' ) . '; }';
	$style[] = '#comments .comments-form input[type=submit]:hover, #commentsSection .comments-form input[type=submit]:hover { background: ' . get_theme_mod( 'primary_color' ) . '; }';

	// Bump
	$style[] = '.scroll-down.scroll-top-active .scroll-arrow:hover { border-color: ' . get_theme_mod( 'primary_color' ) . '; }';
	$style[] = '.scroll-down.scroll-top-active .scroll-arrow:hover .line { background: ' . get_theme_mod( 'primary_color' ) . '; }';

	// Site Header
	$style[] = 'header .header-right li a:after { background: ' . get_theme_mod( 'primary_color' ) . '; }';

	// Portfolio
	$style[] = '#portfolioSection .portfolio-nav a:after { background: ' . get_theme_mod( 'primary_color' ) . '; }';

	// Blog
	$style[] = '.sticky .featured { background: ' . get_theme_mod( 'primary_color' ) . '; }';
	
	// Single Post
	$style[] = '#singleNews .single-news-left .tags a, #singleNews .single-news-left .tags span { border-color: ' . get_theme_mod( 'primary_color' ) . '; }';
	$style[] = '#singleNews .single-news-left .tags a:hover, #singleNews .single-news-left .tags span:hover { background: ' . get_theme_mod( 'primary_color' ) . '; }';

	// Commments
	$style[] = '#comments .comments-form textarea:focus, #commentsSection .comments-form textarea:focus { border-color: ' . get_theme_mod( 'primary_color' ) . '; }';
	$style[] = '#comments .comments-form input:focus, #commentsSection .comments-form input:focus { border-color: ' . get_theme_mod( 'primary_color' ) . '; }';



	// Pagination
	$style[] = '.pagination .nav-links a.current, .pagination .nav-links span.current { background: ' . get_theme_mod( 'primary_color' ) . '; }';
	$style[] = '.pagination .nav-links a, .pagination .nav-links span { border-color: ' . get_theme_mod( 'primary_color' ) . '; }';
	$style[] = '.pagination .nav-links a:hover, .pagination .nav-links span:hover { background: ' . get_theme_mod( 'primary_color' ) . '; }';
	$style[] = '#comments .comment-respond .comment-form p.comment-form-cookies-consent input[type=checkbox]:checked+label:before { background: ' . get_theme_mod( 'primary_color' ) . '; border-color: ' . get_theme_mod( 'primary_color' ) . '; }';


	// Elemmentor TODO
	$style[] = '#aboutTeam .team-row .team-box .team-box-title a:hover { color: ' . get_theme_mod( 'primary_color' ) . ' !important; }';

	$style[] = '#contactSection .contact-right input:focus { border-color: ' . get_theme_mod( 'primary_color' ) . '; }';
	$style[] = '#contactSection .contact-right textarea:focus { border-color: ' . get_theme_mod( 'primary_color' ) . '; }';
	$style[] = '#contactSection .contact-right input[type=submit] { border-color: ' . get_theme_mod( 'primary_color' ) . '; }';
	$style[] = '#contactSection .contact-right input[type=submit]:hover { background: ' . get_theme_mod( 'primary_color' ) . '; }';


	echo "<style>\n" . implode( "\n", $style ) . "\n</style>\n";
}

add_action( 'wp_head', 'customizer_style_tag' );
