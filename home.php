<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 * @author Artem Horbenko <artemhorbenko@gmail.com>
 * @package Strada
 */

get_header();
?>

	<!---------- Hero Start ---------->
	<section id="heroSection" class="section parallax-top-section">

		<div class="container-fluid-small">

			<div class="row">

				<div class="col-12">
					<h6 class="sub-title">Latest News</h6>
					<h1 class="big-title">We're not just boring developers <br> We love writting as well.</h1>
				</div>

			</div>

		</div>

	</section>
	<!---------- Hero End ---------->

	<!---------- News Start ---------->
	<section id="newsSection" class="section">

		<div class="container-fluid-small">

			<div class="row">

				<?php if ( have_posts() ) : ?>
					<?php
					/* Start the Loop */
					while ( have_posts() ) :
						the_post();

						/*
						 * Include the Post-Type-specific template for the content.
						 * If you want to override this in a child theme, then include a file
						 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
						 */
						get_template_part( 'template-parts/content-news', get_post_type() );

					endwhile;

					strada_pagintion();

				else :

					get_template_part( 'template-parts/content', 'none' );

				endif;
				?>

			</div>

		</div>

	</section>
	<!---------- News End ---------->

<?php
get_footer();
