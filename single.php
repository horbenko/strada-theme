<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 * @author Artem Horbenko <artemhorbenko@gmail.com>
 * @package Strada
 */

get_header();

$strada_single_title_classes = 'big-title';

if ( strlen( get_the_title() ) > 60 ) :
	$strada_single_title_classes .= ' break-word';
endif;
?>

	<!---------- Hero Start ---------->
	<section id="heroSection" class="simple-hero single-news-hero section">
		<div class="container-fluid-small">
			<h6 class="sub-title"><span class="single-news-date"><?php strada_posted_data(); ?></span><?php strada_entry_categories(); ?></h6>
			<?php the_title( '<h1 class="' . $strada_single_title_classes . '">', '</h1>' ); ?>
		</div>

	</section>
	<!---------- Hero End ---------->

	<!---------- News Start ---------->
	<section id="singleNews" class="section">

		<div class="container-fluid-small">

			<div class="row">
				<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-8 single-news-left">
					<?php
					while ( have_posts() ) :
						the_post();

						get_template_part( 'template-parts/content-single', get_post_type() );

					endwhile; // End of the loop.
					?>
				</div>

				<div class="col-12 col-sm-8 col-md-8 col-lg-6 col-xl-4 single-news-right sidebar">
					<?php
					get_sidebar();
					?>
				</div>

				<div class="col-12 col-sm-4 col-md-4 col-lg-6"></div>
			</div>

		</div>

	</section>
	<!---------- News End ---------->


	<!---------- Post Navigation Start ---------->
	<section id="postNav">

		<div class="container-fluid-small">

			<div class="row">

				<div class="col-4 post-nav-left post-nav anim-bot">
					<h1 class="big-title"><?php previous_post_link( '%link', 'Prev Post' ); ?></h1>
				</div>

				<div class="col-4 post-nav-center post-nav anim-bot">
					<h1 class="big-title">
						<?php 
						$strada_single_category = get_the_category(); 
						printf(
							'<a href="%s" class="link link_text">Back to %s</a>',
							esc_url( get_category_link( $strada_single_category[0] ) ),
							esc_html( $strada_single_category[0]->name )
						);
						?>
					</h1>
				</div>

				<div class="col-4 post-nav-right post-nav anim-bot">
					<h1 class="big-title"><?php next_post_link( '%link', 'Next Post' ); ?></h1>
				</div>

			</div>

		</div>

	</section>
	<!---------- Post Navigation End ---------->

	<?php
	// If comments are open or we have at least one comment, load up the comment template.
	if ( comments_open() || get_comments_number() ) :
		comments_template();
	endif;
	?>

<?php
get_footer( 'blank' );
