<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 * @author Artem Horbenko <artemhorbenko@gmail.com>
 * @package Strada
 */

get_header();
?>

	<!---------- Hero Start ---------->
	<section id="heroSection" class="simple-hero section page-404">

		<div class="container-fluid-small">

			<div class="row">

				<div class="col-12">
					<h6 class="sub-title">Nothing Found <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">Go Home</a></h6>
					<h1 class="big-title">Ooops... Seems like you got lost.<br/>Try search around the site:</h1>
					<h1>
						<!-- <input type="search" placeholder="Type and hit enter..."> -->
						<?php get_search_form(); ?>
					</h1>
				</div>

			</div>

		</div>

	</section>
	<!---------- Hero End ---------->

<?php
get_footer( 'blank' );
