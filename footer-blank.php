<?php
/**
 * The template for displaying the blank footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 * @author Artem Horbenko <artemhorbenko@gmail.com>
 * @package Strada
 */

?>

<?php wp_footer(); ?>

</body>
</html>
